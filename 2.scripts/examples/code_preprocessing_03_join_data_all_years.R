
#############################################################
# MOD3 - Full table
# rm(list = ls())
# devtools::install_github("allanjust/aodlur")
library(gdalUtils)
library(devtools)
install_github("allanjust/aodlur", dependencies = TRUE)
library("aodlur")
library(magrittr)
library(rgdal)
library(sp)
library(rgeos)
library(raster)
library(mapview)
library(plyr)
library(reshape2)
library(scales)
library(parallel)
library(sf)
library(gstat)
setwd("/media/qnap/Data/Echo")

# Read grid
grid = readRDS("./NEMIA.grid.01022017/grid_pnt.rds") # point grid

# # Check that all 'aodid*date' combinations are present - ANSWER IS NO...
# z = nrow(unique(dat[, c("aodid", "day")])) # Unique combinations in data
# x = length(unique(dat$day))
# y = length(unique(dat$aodid))
# x * y # Unique combinations possible

# Create template with all combinations
template = expand.grid(
  day = seq(from = as.Date("2008-01-01"), to = as.Date("2008-12-31"), by = 1),
  aodid = unique(grid$aodid),
  stringsAsFactors = FALSE
)

# Add lon-lat
template = dplyr::left_join(template, as.data.frame(grid)[, c("aodid", "lon", "lat")], c("aodid"))

# Add x-y
us_atlas = "+proj=laea +lat_0=45 +lon_0=-100 +x_0=0 +y_0=0 +a=6370997 +b=6370997 +units=m +no_defs"
grid_us = spTransform(grid, us_atlas)
grid$x = coordinates(grid_us)[, 1]
grid$y = coordinates(grid_us)[, 2]
template = dplyr::left_join(template, as.data.frame(grid)[, c("aodid", "x", "y")], c("aodid"))

# Load AOD data (one year only, 2008)
tmp = readRDS("./yearly.aod.16022017/MAIACAAOT_roi_2008_v2017-03-25.rds")
tmp$roirow = NULL
tmp$roicol = NULL

# Clean aod observations suspected in cloudiness\water - by MAIAC QA flags

tmp$CloudMask = mclapply(
  tmp$AOT_QA,
  function(x){paste(rev(as.integer(intToBits(x))[1:3]), collapse = "")},
  mc.cores = 2
) %>% simplify2array

tmp$MaskLandWaterSnow = mclapply(
  tmp$AOT_QA,
  function(x){paste(rev(as.integer(intToBits(x))[4:5]), collapse = "")},
  mc.cores = 2
) %>% simplify2array

tmp$MaskAdjacency = mclapply(
  tmp$AOT_QA,
  function(x){paste(rev(as.integer(intToBits(x))[6:8]), collapse = "")},
  mc.cores = 2
) %>% simplify2array

tmp$CloudDetection = mclapply(
  tmp$AOT_QA,
  function(x){paste(rev(as.integer(intToBits(x))[9:12]), collapse = "")},
  mc.cores = 2
) %>% simplify2array

tmp$GlintMask = mclapply(
  tmp$AOT_QA,
  function(x){paste(rev(as.integer(intToBits(x))[13]), collapse = "")},
  mc.cores = 2
) %>% simplify2array

tmp$AerosolModel = mclapply(
  tmp$AOT_QA,
  function(x){paste(rev(as.integer(intToBits(x))[14:15]), collapse = "")},
  mc.cores = 2
) %>% simplify2array

# Make sure that the values that were created are reasonable
# unique(tmp$CloudMask)
# unique(tmp$MaskLandWaterSnow)
# unique(tmp$CloudDetection)
# unique(tmp$AerosolModel)
# unique(tmp$GlintMask)
# unique(tmp$MaskAdjacency)
gc()

# Check if there is duplicated data
tmp2 <-tmp %>%
  dplyr::group_by(aodid,day) %>%
  dplyr::summarise(aod=mean(aod,na.rm=TRUE))

nrow(tmp2) == nrow(tmp)
rm(tmp2)

## If FALSE run the QA cleaning in this stage, if TRUE this cleaning stage can be done later with the rest of the cleaning procedures procedures

# library(dplyr)
# # remove cloudy AOT_QA
# tmp=filter(tmp,CloudMask!="011")
# tmp=filter(tmp,CloudMask!="010")
# # remove observatiobs surrounded  by more than 8 cloudy pixels QA
# tmp=filter(tmp,MaskAdjacency!="010")
# # remove observatiobs Adjacent to cloud & snow
# tmp=filter(tmp,MaskAdjacency=="000")
# # remove Adjacent to snow QA
# tmp=filter(tmp,MaskAdjacency!="100")
# # remove water QA
# tmp=filter(tmp,MaskLandWaterSnow=="00") # Keep only Land AOD (00)
# # remove Glint observations
# tmp=filter(tmp,GlintMask!="1")
# # remove observatiobs Adjacent to cloud
# tmp=filter(tmp,CloudDetection=="0000")

# Join with AOD data (one year only, 2008)
template = dplyr::left_join(template, tmp, c("aodid", "day"))

# Join with NARR
tmp = readRDS("./NOAA.NARR.daily.09022017/NARR_2008.rds")
tmp$roirow = NULL
tmp$roicol = NULL
tmp = rename(tmp, c("date" = "day"))
template = dplyr::left_join(template, tmp, c("aodid", "day"))

# Join with elevation
tmp = readRDS("./grid_1km_elev.rds")
tmp$hdfrow  = NULL
tmp$hdfcol = NULL
tmp$tile = NULL
tmp$roirow = NULL
tmp$roicol = NULL
template = dplyr::left_join(template, tmp, c("aodid"))

# Join with population density
tmp = readRDS("./grid_1km_pop_dens.rds")
tmp$hdfrow  = NULL
tmp$hdfcol = NULL
tmp$tile = NULL
tmp$roirow = NULL
tmp$roicol = NULL
template = dplyr::left_join(template, tmp, c("aodid"))

# Join with land cover
files = list.files(
  path = "./NLCD_2008_05022017/nlcd_2008_landcover_2008_edition_2014_10_10/LC_Proprtion/",
  # pattern = "\\.rds$",
  pattern = "1km.*\\.rds$",
  full.names = TRUE
)
for(i in files) {
  tmp = readRDS(i)
  tmp$hdfrow  = NULL
  tmp$hdfcol = NULL
  tmp$tile = NULL
  tmp$roirow = NULL
  tmp$roicol = NULL
  template = dplyr::left_join(template, tmp, c("aodid"))
}

# Join with roads data
tmp = readRDS("./grid_1km_roads.rds")
tmp$hdfrow  = NULL
tmp$hdfcol = NULL
tmp$tile = NULL
tmp$roirow = NULL
tmp$roicol = NULL
template = dplyr::left_join(template, tmp, c("aodid"))

# Join climatic zones

# Load regions and transform their coordinate system
regions=readOGR("/media/qnap/Data/Echo/regions/","simple.regions")
regions.ts = spTransform(
  regions,
  "+proj=laea +lat_0=45 +lon_0=-100 +x_0=0 +y_0=0 +a=6370997 +b=6370997 +units=m +no_defs"
)
# Transform the grid coordinate system
grid.ts = spTransform(
  grid,
  "+proj=laea +lat_0=45 +lon_0=-100 +x_0=0 +y_0=0 +a=6370997 +b=6370997 +units=m +no_defs"
)
library("spatialEco")
grid.poly <- point.in.poly(grid.ts, regions.ts)
head(grid.poly@data)
summary(grid.poly@data$reg)

# Add the climatic zones info to the template database
grid.poly=as.data.frame(grid.poly)
grid.poly.s=grid.poly[,c("aodid","reg")]
template = dplyr::left_join(template, grid.poly.s, c("aodid"))

gc()

# Add PM2.5 data to the db- from IDW interpolation

pmall=readRDS("/media/qnap/Data/Echo/PM.stations.050227017/ALL_PM_STATIONS_MERGE.rds")# Load the PM data
pmall$year=format(pmall$Date,"%Y")
stations=dplyr::filter(pmall, year=="2008")# Filter the data according to the current year

stations <-stations %>%
  dplyr::group_by(stn,Date) %>%
  dplyr::summarise(pm25=mean(pm25,na.rm=TRUE),lat.stn=mean(lat.stn,na.rm=TRUE),long.stn=mean(long.stn,na.rm=TRUE))

stations=as.data.table(stations)
# Calculate US-Atlas coordinates - For stations
stations_pnt = as.data.frame(stations)
coordinates(stations_pnt) = ~ long.stn + lat.stn
proj4string(stations_pnt) = "+proj=longlat +datum=WGS84 +no_defs +ellps=WGS84 +towgs84=0,0,0"
stations_pnt = spTransform(
  stations_pnt,
  "+proj=laea +lat_0=45 +lon_0=-100 +x_0=0 +y_0=0 +a=6370997 +b=6370997 +units=m +no_defs"
)
stations$x = coordinates(stations_pnt)[, 1]
stations$y = coordinates(stations_pnt)[, 2]

gc()


# Prepare a list for the IDW loop

template.s=template[template$day == template$day[1], c("aodid","x","y")]

final = list()

# count=unique(template$day)
for(i in unique(as.character(template$day))) {
  x=stations[stations$Date==i, ]
  # y=template[template$day==i, ]
  library(gstat)
  inter = gstat(formula = pm25 ~ 1,  locations = ~ x + y, data = x)
  # z<-predict(object = inter, newdata = template.s)
  # template$pred[template$day==i] = z$var1.pred
  # tmp= template[template$day==i,]
  z<-predict(object = inter, newdata = template.s)
  template.s$pred = z$var1.pred
  template.s$day = i
  final[[i]] = template.s
  # tmp= template[template$day==i,]
  # gc(verbose=FALSE)
}

template.s = do.call(rbind.fill, final)
template.s=template.s[,c("aodid","pred","day")]
template.s$day=as.Date(template.s$day)
template= dplyr::left_join(template, template.s, c("aodid", "day"))
colnames(template)[colnames(template)=="pred"] <- "PM25_IDW"

# Add closest AERONET data location
# aeronet=read.csv("/media/qnap/Data/Echo/AERONET_data/aeronet_data_for_join.csv")
# aeronet$day=as.Date(aeronet$day)
# aeronet$aodid=as.character(aeronet$aodid)
# aeronet=aeronet[,c("Site_Name","day","lon","lat","aodid")]
# names(aeronet)[1] <- paste("aeronet.s.name")
# names(aeronet)[3] <- paste("lon.aeronet")
# names(aeronet)[4] <- paste("lat.aeronet")
# template = dplyr::left_join(template, aeronet, c("aodid","day"))

# Save result - MOD3
saveRDS(template, "./yearly.aod.16022017/MOD1&2&3/MAIACAAOT_roi_2008_v2017-03-25_MOD3.rds")

#############################################################
# MOD2 - Filter MOD3 to keep only points with satellite AOD

mod2 = template[!is.na(template$aod), ]

# Scale spatial- temproal variables

names=names(mod2)
names=names[26:length(names)]
scaled =mod2[,names] %>% dplyr::mutate_each(dplyr::funs(scale))
colnames(scaled) = paste0(colnames(scaled), ".s")
mod2= cbind(mod2, scaled)
names(mod2)

# Save result - MOD2
saveRDS(mod2, "./yearly.aod.16022017/MOD1&2&3/MAIACAAOT_roi_2008_v2017-03-25_MOD2.rds")

#############################################################
# MOD1 - PM2.5 from ground stations along with nearest satelline measurement (up to 1.5 km)

library(data.table)
stations=as.data.table(stations)
setnames(stations,"Date","day")



- Kloog, I., Melly, S. J., Coull, B. A., Nordio, F., & Schwartz,
  J. D. (2015). Using satellite-based spatiotemporal resolved air
  temperature exposure to study the association between ambient air
  temperature and birth outcomes in massachusetts. , (),
  . http://dx.doi.org/10.1289/ehp.1308075

- Sade, M. Y., Novack, V., Ifergane, G., Horev, A., & Kloog,
  I. (2015). Air pollution and ischemic stroke among young
  adults. Stroke, 46(12),
  3348–3353. http://dx.doi.org/10.1161/strokeaha.115.010992

- Kloog, I., Coull, B. A., Zanobetti, A., Koutrakis, P., & Schwartz,
  J. D. (2012). Acute and chronic effects of particles on hospital
  admissions in new-England. , 7(4),
  34664. http://dx.doi.org/10.1371/journal.pone.0034664

- Sade, M. Y., Kloog, I., Liberty, I. F., Schwartz, J., & Novack,
  V. (2016). The association between air pollution exposure and
  glucose and lipids levels. The Journal of Clinical Endocrinology \&
  Metabolism, (), 2016–1378. http://dx.doi.org/10.1210/jc.2016-1378


                                        # Nearest-by-day join

# Create the needed names, fields and format for the nearestbyday function
# mod2$aodid=paste(formatC(round(mod2$lon,4),format='f',4),formatC(round(mod2$lat,4),format='f',4),sep="_")
mod2=as.data.table(mod2)

jointo.pt = makepointsmatrix(datatable = stations, xvar = "x", yvar = "y", idvar = "stn")
joinfrom.pt = makepointsmatrix(datatable = mod2, xvar = "x", yvar = "y", idvar = "aodid")

joinout = nearestbyday(jointo.pts = jointo.pt, joinfrom.pts = joinfrom.pt,
                       jointo = stations, joinfrom = mod2,
                       jointovarname = "stn", joinfromvarname = "aodid",
                       joinprefix = "nearest", valuefield = "aod",
                       knearest = 9, maxdistance = 1500,
                       nearestmean = FALSE, verbose = TRUE)

setkey(stations, stn, day)
setkey(joinout, stn, day)
stations = merge(stations, joinout,all.x = TRUE)
setnames(stations,"x.x","x.stn")
setnames(stations,"y.x","y.stn")
setnames(stations,"x.y","x.aod")
setnames(stations,"y.y","y.aod")

# Remove station measurements that were not joined to satellite AOD
stations = as.data.frame(stations)
stations = stations[!is.na(stations$aod), ]
stations = as.data.table(stations)
# Save result - MOD1
saveRDS(stations, "./yearly.aod.16022017/MOD1&2&3/MAIACAAOT_roi_2008_2017-02-24_MOD1_1500.rds")
























