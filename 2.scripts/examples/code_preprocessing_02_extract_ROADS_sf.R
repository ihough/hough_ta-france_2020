library(magrittr)
library(leaflet)
library(sp)
library(rgdal)
library(rgeos)
library(raster)
library(parallel)
library(plyr)
library(sf)

# Load grid
setwd("/media/qnap/Data/Echo")

us_atlas = "+proj=laea +lat_0=45 +lon_0=-100 +x_0=0 +y_0=0 +a=6370997 +b=6370997 +units=m +no_defs"

# Load AOI
aoi = st_read("./AOI/", "NE_MIA_20kbuf")

# Load Grid
grid_1km = readRDS("./NEMIA.grid.01022017/grid_square_1km.rds") %>% st_as_sf # square grid

# Load roads
roads = st_read("./OSM01012017/", "NEMIA_roads", stringsAsFactors = FALSE)
roads = st_transform(roads, us_atlas)

# Load railways
railways = st_read("./OSM01012017/", "NEMIA_railway", stringsAsFactors = FALSE)
railways = st_transform(railways, us_atlas)

# Reclassify roads
major = c("primary", "motorway", "trunk")
major = c(major, paste0(major, "_link"))
roads_major = roads[roads$fclass %in% major, ]

# Union
Sys.time()
railways = st_union(railways)
Sys.time()
roads = st_union(roads)
Sys.time()
roads_major = st_union(roads_major)
Sys.time()

save.image("~/Downloads/roads_tmp.RData")

###############################################################################
# Road density (length / km^2)

library(magrittr)
library(leaflet)
library(sp)
library(rgdal)
library(rgeos)
library(raster)
library(parallel)
library(plyr)
library(sf)

# Load grid
setwd("/media/qnap/Data/Echo")

load("~/Downloads/roads_tmp.RData")

# # Tests
# st_write(railways, "/home/michael/Downloads/", "railways", driver = "ESRI Shapefile")
# st_write(grid_1km, "/home/michael/Downloads/", "grid_1km", driver = "ESRI Shapefile")
# st_write(roads_major, "/home/michael/Downloads/", "NEMIA_roadsmajor_dissolve", driver = "ESRI Shapefile")
# 
# aoi = st_read("~/Dropbox/Projects/Flickr_Emotion_Analysis_Paper_3_Golan/AOI_zoom_composite_map.json", "OGRGeoJSON")
# aoi = st_transform(aoi, st_crs(railways)$proj4string)
# 
# # Test
# rail = railways[aoi, ]
# rail = st_union(rail)
# grid = grid_1km[rail, ]
# 
# x = st_intersection(grid[2,], rail)
# x %>% st_length %>% as.numeric

# Set parallel
library(foreach)
library(doParallel)
cl = makeCluster(detectCores()-16)
registerDoParallel(cl)

# Actual code
roads_lengths = foreach(
  i = 1:nrow(grid_1km), 
  # .combine = c, 
  .packages = c("sf", "magrittr")
) %dopar% {
  
  railways_length_km_km2 = st_intersection(railways, grid_1km[i, ])
  railways_length_km_km2 = if(length(railways_length_km_km2) > 0) {
    railways_length_km_km2 %>% st_length %>% as.numeric %>% divide_by(1000)
    } else 0
  
  roads_length_km_km2 = st_intersection(roads, grid_1km[i, ])
  roads_length_km_km2 = if(length(roads_length_km_km2) > 0) {
    roads_length_km_km2 %>% st_length %>% as.numeric %>% divide_by(1000)
    } else 0
  
  majorroads_length_km_km2 = st_intersection(roads_major, grid_1km[i, ])
  majorroads_length_km_km2 = if(length(majorroads_length_km_km2) > 0) {
    majorroads_length_km_km2 %>% st_length %>% as.numeric %>% divide_by(1000)
    } else 0
  
  nearest_road_dist_km = st_distance(railways, grid_1km[i, ]) %>% as.numeric %>% divide_by(1000)
  
  c(railways_length_km_km2, roads_length_km_km2, majorroads_length_km_km2, nearest_road_dist_km)
  
}

stopCluster(cl)

dat$railways_length_m_km2 = sapply(roads_lengths, "[", 1)
dat$roads_length_m_km2 = sapply(roads_lengths, "[", 2)
dat$majorroads_length_m_km2 = sapply(roads_lengths, "[", 3)
dat$nearest_road_dist_km = sapply(roads_lengths, "[", 4)

# Save result
saveRDS(dat, "aod_basegrid_final_ROADS_LU_ELEV_NDVI.rds")





