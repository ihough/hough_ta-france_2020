library(magrittr)
library(leaflet)
library(mapview)
library(sp)
library(rgdal)
library(rgeos)
library(raster)
library(parallel)
library(plyr)
library(velox)

# Load grid
setwd("/media/qnap/Data/Echo")

# Load AOI
aoi = readOGR("./AOI/", "NE_MIA_20kbuf")
s = gSimplify(aoi, tol = 0.005)

# Load Grid
# grid = readRDS("./NEMIA.grid.01022017/grid_pnt.rds") # point grid
grid_1km = readRDS("./NEMIA.grid.01022017/grid_square_1km.rds") # square grid

###############################################################################
# Population data

r = raster("/media/qnap/Data/Echo/Gridded_World_Population_2010/gpw-v4-population-density-adjusted-to-2015-unwpp-country-totals_2010.tif")

# Reproject 'grid' and 'AOI' to CRS of raster
grid_1km_proj = spTransform(grid_1km, proj4string(r))

# Crop raster
aoi = spTransform(aoi, proj4string(r))
r = crop(r, aoi, progress = "text")

# Extract
vx = velox(r) # 'RasterLayer' to 'velox' object
f = function(x) mean(x, na.rm = TRUE)
x = vx$extract(sp = grid_1km_proj, fun = f) # 'Fast' extraction method
grid_1km$pop_dens_2015 = x[, 1] # Add new column in 'grid_1km' layer

# Save the result and clean memory
grid_1km_t = as.data.frame(grid_1km)
saveRDS(grid_1km_t, "/media/qnap/Data/Echo/grid_1km_pop_dens.rds")





