# Display a message with a timestamp and also write it to the logfile, if one exists
report <- function(msg) {

  # Format msg if it is a table
  if (is.data.frame(msg)) {

    max <- 100
    omit <- nrow(msg) > max
    if (omit) {
      msg <- msg[seq_len(max), ]
    }

    msg <- rbind(
             names(msg),
             format(msg)
           ) %>%
           apply(2, format, justify = "right") %>%
           apply(1, paste, collapse = " ")

    if (omit) {
      msg <- c(
               msg,
               paste0("[ truncated ]")
             )
    }

  }

  txt <- paste(Sys.time(), msg, sep = ": ")
  lapply(txt, message)
  if (exists("constants") && !is.null(constants$logfile)) {
    write(txt, constants$logfile, append = TRUE)
  }

}
