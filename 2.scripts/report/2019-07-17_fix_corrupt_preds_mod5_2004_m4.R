# Explore and fix unloadable file containing final 200 m predictions for 2004

library(magrittr)
library(data.table)
library(fst)
library(purrr)
library(dplyr)
library(sf)
library(ggplot2)

# Load corrupted file mod5_2004_pred_m4.fst
# Only the grd_200m_id column is unreadable
corrupt_path <- "/media/summer_houghi/P055.FR.Ta/1.work/predictions/mod5_2004_pred_m4.fst.bad"
corrupt <- list(
             read_fst(corrupt_path, as.data.table = TRUE, to = 8871698),
             read_fst(corrupt_path, as.data.table = TRUE, from = 8871699, to = 8873745,
                      columns = c("date", "x", "y", "tmin", "tmean", "tmax")),
             read_fst(corrupt_path, as.data.table = TRUE, from = 8873746)
           ) %>%
           rbindlist(fill = TRUE)

# Load the a subset (rows 8000000 to 9000000) including the corrupted rows as read and resaved on
# Emie's macbook
emie <- read_fst("/media/summer_houghi/P055.FR.Ta/1.work/predictions/mod5_2004_pred_m4_rows_8m_9m_emie_18072019.fst",
                 as.data.table = TRUE)

# Confirm the data is identical, except for the corrupted grd_200m_id column
idx <- 8000000:9000000
names(corrupt) %>%
  print %>%
  map_chr(.f = ~ all.equal(corrupt[[.x]][idx], emie[[.x]]))
# [1] "date" "grd_200m_id" "x" "y" "tmin" "tmean" "tmax"
# [1] "TRUE"
# [2] "'is.NA' value mismatch: 0 in current 2047 in target"
# [3] "TRUE"
# [4] "TRUE"
# [5] "TRUE"
# [6] "TRUE"
# [7] "TRUE"

# For the corrupted section, replace grd_200m_id with the values from Emie's file
corrupt[idx, grd_200m_id := emie$grd_200m_id, ]
rm(emie)

# Spot check some rows
stopifnot(
  identical(
    fst(corrupt_path)[7999999, ],
    corrupt[7999999, ] %>% as.data.frame
  )
)
stopifnot(
  identical(
    fst(corrupt_path)[8000000, ],
    corrupt[8000000, ] %>% as.data.frame
  )
)
stopifnot(
  identical(
    fst(corrupt_path)[8871699, c("date", "x", "y", "tmin", "tmean", "tmax")],
    corrupt[8871699, c("date", "x", "y", "tmin", "tmean", "tmax")] %>% as.data.frame
  )
)
stopifnot(
  identical(
    fst(corrupt_path)[8872700, c("date", "x", "y", "tmin", "tmean", "tmax")],
    corrupt[8872700, c("date", "x", "y", "tmin", "tmean", "tmax")] %>% as.data.frame
  )
)
stopifnot(
  identical(
    fst(corrupt_path)[8873745, c("date", "x", "y", "tmin", "tmean", "tmax")],
    corrupt[8873745, c("date", "x", "y", "tmin", "tmean", "tmax")] %>% as.data.frame
  )
)
stopifnot(
  identical(
    fst(corrupt_path)[8873746, c("date", "x", "y", "tmin", "tmean", "tmax")],
    corrupt[8873746, c("date", "x", "y", "tmin", "tmean", "tmax")] %>% as.data.frame
  )
)
stopifnot(
  identical(
    fst(corrupt_path)[9000000, ],
    corrupt[9000000, ] %>% as.data.frame
  )
)
stopifnot(
  identical(
    fst(corrupt_path)[9000001, ],
    corrupt[9000001, ] %>% as.data.frame
  )
)

# Uh oh, on Feb 6 (the corrupted section) some cells have grd_1km_id that does not correspond to the
# grid cell coordinates
check <- corrupt[idx, ]
xy <- st_as_sf(check, coords = c("x", "y"), crs = 3035) %>%
      st_transform(crs = 4326) %>%
      st_coordinates %>%
      as.data.frame
check[, c("lon", "lat") := xy]
rm(xy)
bad <- check[round(lat, 6) != substr(grd_200m_id, 1, 9) %>% as.numeric, ]
bad
# 2047 rows

# Map the points
roi <- st_read("1.work/borders/france_epsg-3035.shp")
pts <- st_as_sf(bad, coords = c("x", "y"), crs = 3035)

mapview::mapview(pts, zcol = "tmean")

ggplot() +
  geom_sf(data = roi) +
  geom_sf(data = pts, aes(colour = tmean)) +
  scale_color_gradient2(low = "blue", high = "red")
dir.create("3.outcomes/2019-07-17_fix_corrupt_preds_mod5_2004")
ggsave("3.outcomes/2019-07-17_fix_corrupt_preds_mod5_2004/bad_pts.png", height = 4.5, width = 7)


# Load a new predictions file made by applying m4 to mod5_2004
new_preds <- read_fst("1.work/predictions/mod5_2004_pred_m4.fst.new", as.data.table = TRUE)

# Compare the new predictions to the corrupted file
map_chr(names(corrupt), ~ all.equal(corrupt[[.x]], new_preds[[.x]]))
# [1] "TRUE"
# [2] "2047 string mismatches"
# [3] "TRUE"
# [4] "TRUE"
# [5] "TRUE"
# [6] "TRUE"
# [7] "TRUE"

# Confirm the new predictions grd_200m_id matches the grid cell coordinates
idx <- which(corrupt$grd_200m_id != new_preds$grd_200m_id)
xy <- new_preds[idx, ] %>%
      st_as_sf(coords = c("x", "y"), crs = 3035) %>%
      st_transform(crs = 4326) %>%
      st_coordinates %>%
      as.data.frame
new_preds[idx, c("lon", "lat") := xy]
rm(xy)
new_preds[idx, ] %>%
  .[round(lat, 6) != substr(grd_200m_id, 1, 9) %>% as.numeric, ]
# Empty data.table (0 rows and 9 cols): date,grd_200m_id,x,y,tmin,tmean...
