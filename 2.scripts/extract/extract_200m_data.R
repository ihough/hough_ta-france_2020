# Extract data to the 200 m grid

# Load packages, helper functions, and constants
source("init.R")
source("2.scripts/extract/post_process_clc.R")

report("---")
report("Extracting all datasets to the 200 m grid")


# Setup ------------------------------------------------------------------------

# Paths to the reference grid to which data will be extracted
grd_pts_path <- "1.work/grids/grd_200m_pts.rds" # for grid ID and to extract by points
grd_pys_path <- "1.work/grids/grd_200m_pys.rds" # to extract by polygons
grd_rst_path <- "1.work/grids/grd_200m_pxs.tif" # to check if data is perfectly aligned to grid

# The datasets to extract
datasets <- list(
  list(
    name = "climate",
    description = "Joly (2010) climate types",
    data_path = "1.work/climate_types/joly_2010_climate_types_50m.tif",
    out_path = "1.work/extracts/grd_200m_climate.fst",
    extract_fun = function(x) { modal(x, na.rm = TRUE) },  # predominant climate type
    post_process = function(x, ...) { as.factor(x) } # convert to factor
  ),
  list(
    name = "elev",
    description = "EU-DEM v1.1",
    data_path = "1.work/elevation/eu-dem_v1-1/eu_dem_v11_france.tif",
    out_path = "1.work/extracts/grd_200m_elev.fst",
    extract_fun = function(x) { mean(x) %>% round %>% as.integer } # mean elevation
  ),
  list(
    name = "clc_2000",
    description = "Corine Land Cover 2000",
    data_path = "1.work/land_cover/clc_2000_france.tif",
    out_path = "1.work/extracts/grd_200m_clc_2000.fst",
    post_process = post_process_clc # percent of cell occupied by each clc class
  ),
  list(
    name = "clc_2006",
    description = "Corine Land Cover 2006",
    data_path = "1.work/land_cover/clc_2006_france.tif",
    out_path = "1.work/extracts/grd_200m_clc_2006.fst",
    post_process = post_process_clc # percent of cell occupied by each clc class
  ),
  list(
    name = "clc_2012",
    description = "Corine Land Cover 2012",
    data_path = "1.work/land_cover/clc_2012_france.tif",
    out_path = "1.work/extracts/grd_200m_clc_2012.fst",
    post_process = post_process_clc # percent of cell occupied by each clc class
  ),
  list(
    name = "pop",
    description = "INSEE 200 m gridded 2010 population",
    data_path = "1.work/population/insee_population_200m.tif",
    out_path = "1.work/extracts/grd_200m_pop.fst",
    post_process = function(x, ...) { replace(x, is.na(x), 0) %>% as.integer } # NA means pop = 0
  )
)

# Extract ----------------------------------------------------------------------

# Extract the datasets in sequence
for (params in datasets) {

  report(params$description)

  # Skip the dataset if it has already been extracted
  if (file.exists(params$out_path) & !OVERWRITE) {

    next

  }

  # Load the grid points; these will be used to set the grid id on the extracted data
  if (!exists("grd_pts")) {

    report("  Loading grid points")
    grd_pts <- grd_pts_path %>% readRDS

  }

  # Check whether the data CRS, origin, and resolution match the grid
  # If so we can extract by points which is much faster
  dat <- params$data_path %>% raster
  grd_rst <- grd_rst_path %>% raster
  align_check <- compareRaster(grd_rst, dat, extent = FALSE, rowcol = FALSE, crs = TRUE,
                               res = TRUE, orig = TRUE, stopiffalse = FALSE)
  if (align_check) {

    # Data is aligned with grid; extract by points (much faster)

    # Extract the data
    report("  Extracting by points")
    extracted <- raster::extract(dat, grd_pts)

  } else {

    # Data is not aligned with grid; extract by polygons (slower)

    # Load the grid polygons and split into ncores groups
    if (!exists("grd_pys_splits")) {

      report("  Loading grid polygons")
      grd_pys <- grd_pys_path %>% readRDS
      grd_pys_splits <- split_into_n(grd_pys["id"], ncores)
      rm(grd_pys)

    }

    # Load the data as a velox object for faster extraction
    report("  Loading data with velox")
    vx <- params$data_path %>% velox

    report("  Extracting by polygons")
    extracted <- parallel_velox_extract(vlx = vx, splits = grd_pys_splits, fun = params$extract_fun,
                                        ncores = ncores) %>%
                 unlist(recursive = FALSE, use.names = FALSE)

  }

  # Perform post-processing of the extracted values (e.g. replace NA, convert to factor)
  if (!is.null(params$post_process)) {

    report("  Post-processing extracted data")
    extracted <- params$post_process(extracted, ncores)

  }

  # Convert to a data.table and add the grid id
  if (!is.data.table(extracted)) {
    extracted <- as.data.table(extracted) %>% setnames(params$name)
  }
  extracted[, "grd_200m_id" := grd_pts$id]
  extracted %>% setkey("grd_200m_id") %>%
                names %>%
                setdiff("grd_200m_id") %>%
                c("grd_200m_id", .) %>%
                setcolorder(extracted, .)

  # Save as a raster for visualization
  report("  Saving as raster for visualisation")
  rast <- table_to_raster(extracted, template = grd_pts)
  tif_path <- sub("\\.fst$", ".tif", params$out_path)
  save_geotiff(rast, tif_path, overwrite = OVERWRITE)
  meta_path <- paste0(tif_path, ".bandnames")
  names(rast) %>% as.list %>% fwrite(meta_path, col.names = FALSE)
  paste("  ->", tif_path) %>% report
  rm(rast, tif_path, meta_path)

  # Save data table as fst
  report("  Saving as table")
  tmp_path <- paste0(params$out_path, ".tmp")
  write_fst(extracted, tmp_path, compress = 100)
  file.rename(tmp_path, params$out_path)
  paste("  ->", params$out_path) %>% report
  rm(tmp_path)

  # Cleanup
  if (exists("vx")) { rm(vx) }
  rm(align_check, dat, extracted)

}

# Cleanup
rm(datasets, params, grd_pts_path, grd_pys_path, grd_rst_path, post_process_clc)

report("Done")
