# Create a 200 m grid over metropolitan France (except Corsica) in ETRS89-LAEA Europe (EPSG:3035)
# and choose the cells in which to run the high-resolution temperature model:
# * Contiguous populated urban areas with at least 15000 inhabitants
# * Station locations

# Load packages, helper functions, and constants
source("init.R")

report("---")
report("Creating a 200 m reference grid in ETRS89-LAEA Europe (EPSG:3035) projection")


# Skip if all output files exist
out_paths <- c("grd_200m_inputs.tif", "grd_200m_pts.rds", "grd_200m_pts.shp", "grd_200m_pxs.tif",
               "grd_200m_pys.rds", "grd_200m_pys.shp", "grd_200m.fst", "grd_200m.csv") %>%
             file.path("1.work/grids", .)
skip_if_exist(out_paths)

# 1. Create a 200 m grid over France ------------------------------------------

# Load a shapefile of France in EPSG:3035
report("Loading France in EPSG:3035")
france <- shapefile("1.work/borders/france_epsg-3035.shp")
france_borders <- as(france, "SpatialLines")

# Create a 200 m resolution raster with origin 0,0 that covers France
report("Creating 200 m raster grid covering France")
grd <- raster(
  xmn = xmin(france) %>% divide_by(200) %>% floor   %>% multiply_by(200),
  xmx = xmax(france) %>% divide_by(200) %>% ceiling %>% multiply_by(200),
  ymn = ymin(france) %>% divide_by(200) %>% floor   %>% multiply_by(200),
  ymx = ymax(france) %>% divide_by(200) %>% ceiling %>% multiply_by(200),
  resolution = c(200, 200),
  crs = projection(france)
)

# Create 100 x 100 km regions for processing in parallel
regions <- aggregate(grd, fact = 500, fun = NA, expand = TRUE) %>% as("SpatialPolygons")

# Mask cells that do not intersect France (in parallel)
# ~ 120 seconds
report("Masking cells outside France")
grd <- mclapply(1:length(regions), function(i) {

  # Crop the grid to the region
  region <- regions[i]
  grd_part <- crop(grd, region)

  # If the region is in France
  if (gIntersects(france, region)) {

    # Store grid cell lat/lon
    lon_lat <- as(grd_part, "SpatialPoints") %>%
               spTransform("+init=epsg:4326") %>%
               coordinates %>%
               as.data.frame
    grd_part$lat <- lon_lat$y
    grd_part$lon <- lon_lat$x

    # If the region intersects the border
    if (gIntersects(france_borders, region)) {

      # Rasterize both the polygon and the border - the latter includes cells that intersect the
      # border but whose centroid is outside
      contained <- crop(france, region) %>% rasterize(grd_part$lat)
      on_border <- crop(france_borders, region) %>% rasterize(grd_part$lat)
      in_france <- merge(contained, on_border)

      # Mask cells that are not in France
      grd_part <- mask(grd_part, in_france)

    }

  }

  # Return the cropped grid section
  grd_part

}, mc.cores = ncores, mc.preschedule = FALSE) %>% do.call(merge, .)
names(grd) <- c("lat", "lon")

# 2. Choose the 200 m grid cells to use for modeling --------------------------
# Contiguous populated urban areas with a total population of at least 15000 OR station locations

# Identify populated urban areas
# Use CLC classes 1.1 "Urban fabric" and 1.2.1 "Industrial or commercial units"
# ~ 45 seconds
report("Identifying populated urban areas")
grd$urban <- raster("1.work/land_cover/clc_2012_france.tif") %>%
             crop(grd) %>%
             reclassify(c(4, 255, NA), right = NA) %>%
             aggregate(fact = 2, fun = min) %>%
             mask(x = ., mask = ., updateNA = TRUE) # replace NaN with NA
grd$pop <- raster("1.work/population/insee_population_200m.tif") %>%
           crop(grd) %>%
           extend(grd)
grd$urban_pop <- mask(grd$pop, grd$urban)

# Find contiguous dense urban areas with total population > 50000
# * Identify 200 m cells with population >= 50
# * Buffer by 1 cell in each direction (~ 200 m)
# * Clump cells that are 4-wise connected (i.e. exclude diagonal-only neighbors)
# * Calculate the total population of each clump
# * Exclude clumps with population < 50000
# ~ 20 seconds
report("Finding contiguous dense urban areas with total population > 50000")
grd$clump <- mask(grd$urban_pop, grd$urban_pop < 50, maskvalue = TRUE) %>%
             focal(w = matrix(1, 3, 3), na.rm = T, NAonly = T) %>%
             as.logical %>%
             clump(directions = 4)
grd$clump_pop <- zonal(grd$pop, grd$clump, fun = sum) %>%
                 reclassify(grd$clump, .)
grd$include <- mask(grd$clump_pop, grd$clump_pop < 50000, maskvalue = TRUE) %>%
               as.logical

# # Visualize
#
# # Paris
# vis <- crop(grd, extent(3700000, 3800000, 2845000, 2933000))
# mapview(vis[[c("urban", "urban_pop", "clump_pop", "include")]], alpha.regions = 1, na.color = NA, method = "ngb", maxpixels = 1e6)
#
# # Grenoble
# vis <- crop(grd, extent(3950000, 4010000, 2410000, 2490000))
# mapview(vis[[c("urban", "urban_pop", "clump_pop", "include")]], alpha.regions = 1, na.color = NA, method = "ngb", maxpixels = 1e6)
#
# # Nancy
# vis <- crop(grd, extent(4020000, 4060000, 2830000, 2870000))
# mapview(vis[[c("urban", "urban_pop", "clump_pop", "include")]], alpha.regions = 1, na.color = NA, method = "ngb", maxpixels = 1e6)
#
# # Rennes
# vis <- crop(grd, extent(3435000, 3475000, 2825000, 2865000))
# mapview(vis[[c("urban", "urban_pop", "clump_pop", "include")]], alpha.regions = 1, na.color = NA, method = "ngb", maxpixels = 1e6)
#
# # Brest
# vis <- crop(grd, extent(3225000, 3265000, 2890000, 2930000))
# mapview(vis[[c("urban", "urban_pop", "clump_pop", "include")]], alpha.regions = 1, na.color = NA, method = "ngb", maxpixels = 1e6)
#
# # Poitiers
# vis <- crop(grd, extent(3560000, 3600000, 2635000, 2675000))
# mapview(vis[[c("urban", "urban_pop", "clump_pop", "include")]], alpha.regions = 1, na.color = NA, method = "ngb", maxpixels = 1e6)
#
# # Brive
# vis <- crop(grd, extent(3630000, 3670000, 2470000, 2510000))
# mapview(vis[[c("urban", "urban_pop", "clump_pop", "include")]], alpha.regions = 1, na.color = NA, method = "ngb", maxpixels = 1e6)

# Identify weather station locations (and buffer by 1 cell in each direction)
report("Locating weather stations")
grd$stations <- shapefile("1.work/meteo_obs/clean/station_locations.shp") %>%
                as("SpatialPoints") %>%
                spTransform(proj4string(grd)) %>%
                rasterize(grd) %>%
                focal(w = matrix(1, 5, 5), na.rm = T, NAonly = T)
# mapview(crop(grd$stations, regions[68]), na.color = NA, method = "ngb")

# Identify cells in a populated urban area or near a station
grd$valid <- grd$include | grd$stations

# Save all layers for reference
report("Saving all layers as raster for reference")
path <- "1.work/grids/grd_200m_inputs.tif"
save_geotiff(grd, path, overwrite = TRUE)
paste("->", path) %>% report
bandnames_path <- paste0(path, ".bandnames")
names(grd) %>% as.list %>% fwrite(bandnames_path)

# Mask invalid cells, convert to SpatialPointsDataFrame, and add ID
# ~ 110 seconds
report("Converting valid cells to points and adding ID")
grd <- grd[[c("lat", "lon")]] %>%
       mask(grd$valid)
pts <- rasterToPoints(grd, spatial = TRUE)
pts$id <- paste(sprintf("%.6f", pts$lat), sprintf("%.6f", pts$lon), sep = "/")
pts <- pts[, c("id", "lat", "lon")]


# Save points
report("Saving as points")

# RDS
path <- "1.work/grids/grd_200m_pts.rds"
saveRDS(pts, path)
paste("->", path) %>% report

# SHP
path <- "1.work/grids/grd_200m_pts.shp"
shapefile(pts, path, overwrite = TRUE)


# Save raster
report("Saving as raster")
path <- "1.work/grids/grd_200m_pxs.tif"
save_geotiff(grd, path, overwrite = TRUE)
paste("->", path) %>% report
bandnames_path <- paste0(path, ".bandnames")
names(grd) %>% as.list %>% fwrite(bandnames_path)


# Save as polygons
report("Saving as polygons")
pys <- pts %>% as("SpatialPixelsDataFrame") %>% as("SpatialPolygonsDataFrame")

# RDS
path <- "1.work/grids/grd_200m_pys.rds"
saveRDS(pys, path)
paste("->", path) %>% report

# Shapefile
path <- "1.work/grids/grd_200m_pys.shp"
shapefile(pys, path, overwrite = TRUE)
paste("->", path) %>% report


# Save as data.table
# Do this last b/c keying on id seems to reorder the points in pts!
report("Saving as data.table")
dtable <- as.data.table(pts)
dtable[, x := as.integer(x)]
dtable[, y := as.integer(y)]
setkey(dtable, "id")

# FST
path <- "1.work/grids/grd_200m.fst"
write_fst(dtable, path, compress = 100)
paste("->", path) %>% report

# CSV
path <- "1.work/grids/grd_200m.csv"
fwrite(dtable, path)
paste("->", path) %>% report

report("Done")
