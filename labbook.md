# P055.FR.Ta

Model of 2m air temperature across continental France

## TODO

* Update borders 1 km buffer so that it does not include large land areas in Spain, Andorra, etc.
  (currently Andorra weather stations far from border are included b/c)

## Geographic coordinate reference systems

Most datasets are in one of three coordinate reference systems:

* EPSG:2154 (RGF93 / Lambert-93)
  - Conformal, the official projection for mapping at the scale of continental France
  - https://epsg.io/2154
* EPSG:3035 (ETRS89 / LAEA Europe)
  - Equal-area, widely used for mapping at the scale of Europe
  - https://epsg.io/3035
* EPSG:4326 (WGS84)
  - Standard reference sytem for latitude / longitude
  - https://epsg.io/4326
* MODIS Sinusoidal
  - The sinusoidal projection used by MODIS data
  - https://modis-land.gsfc.nasa.gov/MODLAND_grid.html
* Lambert II étendu
  - A deprecated projection that has been superseded by EPSG:2154, but is still used by some data.
  - See `0.raw/ign/TransformationsCoordonneesGeodesiques.pdf`

## Metadata for `0.raw` (raw data)

### aster

Data from NASA Terra ASTER instrument
https://lpdaac.usgs.gov/dataset_discovery/aster

#### ASTGTM.002

ASTER Global Digital Elevation Model (GDEM)
Version 002
Horizontal resolution 30 m, horizontal accuracy ~= 75 m, vertical RMS ~= 17 m
EPSG:4326 (WGS84) / EPSG:5773 (EGM96 geoid height)
http://dx.doi.org/10.5067/ASTER/ASTGTM.002

```
ASTGTM2_NYYEXXX_dem.tif
      0 = sea water body
      X = elevation (m) above mean sea level
  -9999 = no data

ASTGTM2_NYYEXXX_num.tif
  > 0 = number of ASTER stereo scene pairs used to determine pixel elevation
  < 0 = source of non-ASTER elevation fill data (see astgtm2_readme.pdf for details)

../astgtm2_readme.pdf
  README provided with data download
```

### copernicus

Data from the European Union Copernicus land monitoring service
https://land.copernicus.eu/

#### corine_land_cover

Corine Land Cover (CLC) inventory
Editions 2000, 2006, and 2012
100 m resolution, 44 classes, horizontal accuracy ~= 25 m, thematic accuracy >= 85%
EPSG:3035 (ETRS89 / LAEA Europe)
https://land.copernicus.eu/pan-european/corine-land-cover/

```
g100_clc00_V18_5
  CLC 2000
  https://land.copernicus.eu/pan-european/corine-land-cover/clc-2000/

g100_clc06_V18_5a
  CLC 2006
  https://land.copernicus.eu/pan-european/corine-land-cover/clc-2006/

g100_clc12_V18_5a
  CLC 2012
  https://land.copernicus.eu/pan-european/corine-land-cover/clc-2012/

clc2012_validation_report.pdf
  https://land.copernicus.eu/user-corner/technical-library/clc-2012-validation-report-1

clc2018_nomenclature_illustrated_guide.pdf
  https://land.copernicus.eu/user-corner/technical-library/corine-land-cover-nomenclature-guidelines/html/
```

#### eu-dem

European Digital Elevation Model based on SRTM and ASTER GDEM data
25 m resolution, vertical RMS <= 7 m
EPSG:3035 (ETRS89 / LAEA Europe) / EPSG:5730 (EVRS2000)
http://land.copernicus.eu/pan-european/satellite-derived-products/eu-dem

```
eu-dem_v1-0
  EU-DEM version 1.0, vertical bias = -0.56 m, vertical RMSE = 2.9 m
  http://land.copernicus.eu/pan-european/satellite-derived-products/eu-dem/eu-dem-v1-0-and-derived-products/eu-dem-v1.0

eu-dem_v1-1
  EU-DEM version 1.1 (adjusted for bias, screened artifacts, improved consistency with EU-Hydro)
  http://land.copernicus.eu/pan-european/satellite-derived-products/eu-dem/eu-dem-v1.1

eu-dem_v1-0_statistical_validation.pdfv
  https://land.copernicus.eu/user-corner/technical-library/eu-dem-v1.0

eu-dem_v1-1_user_guide.pdf
  https://land.copernicus.eu/user-corner/technical-library/eu-dem-v1-1-user-guide
```

#### eu-hydro_beta

European Hydrological database derived from SPOT 5, SPOT 6, and other data
Public beta
https://land.copernicus.eu/pan-european/satellite-derived-products/eu-hydro/

```
  Drainage Basins Part 01.gdb
    https://land.copernicus.eu/pan-european/satellite-derived-products/eu-hydro/eu-hydro-public-beta/integrated-eu-hydro-database-hydrographic-and-drainage-database/view

  eu-hydro_2012_validation_report_1_1.pdf
    https://land.copernicus.eu/user-corner/technical-library/eu-hydro-validation-report

  eu-hydro_user_guide_v3.pdf
    https://land.copernicus.eu/user-corner/technical-library/eu-hydro-beta-user-guide
```

#### global_surface_water

Global Surface Water dataset based on Landsat 5 / 7 / 8 data
30 m resolution
EPSG:3035 (ETRS89 / LAEA Europe) / EPSG:5730 (EVRS2000)
https://global-surface-water.appspot.com/

```
data
  https://global-surface-water.appspot.com/download

metadata
  https://global-surface-water.appspot.com/download

symbology
  https://global-surface-water.appspot.com/download

DataUsersGuidev2.pdf
  https://global-surface-water.appspot.com/download

nature20584.pdf
  Nature letter describing dataset and methodology
  https://www.nature.com/articles/nature20584
```

### ign

Data from the Institut National de l'Information Géographique et Foréstière (IGN)
http://professionnels.ign.fr/donnees

```
DC_Metadonnees_2-0.pdf
  Metadata for the geographic reference systems used by IGN products
  http://professionnels.ign.fr/sites/default/files/DC_Metadonnees_2-0.pdf

TransformationsCoordonneesGeodesiques.pdf
  Description of geographic transformations for France
  http://geodesie.ign.fr/contenu/fichiers/documentation/pedagogiques/TransformationsCoordonneesGeodesiques.pdf
```

#### ADMINEXPRESS_1-0__SHP_LAMB93_FXX_2017-04-18

Administrative areas of France (regions, departements, communes, etc.)
Version 1.0 April 2017
For mapping at 1:50000 to 1:200000 scale
EPSG:2154 (RGF93 / Lambert-93)
http://professionnels.ign.fr/adminexpress

#### BDALTIV2_2-0_25M_ASC_LAMB93-IGN69

Digital elevation model of France
Version 2.0
25 m resolution, vertical RMS <= 7 m
EPSG:2154 (RGF93 / Lambert-93) / EPSG:5720 (NGF IGN69 height)
http://professionnels.ign.fr/bdalti

```
****NOTE****
IGN forces you order and download data files by department. This makes it difficult to load the
entire dataset and results in data duplication (many tiles intersect multiple departments). To
facilitate use and reduce storage volume, all data files have been moved into one of five dirs
according to file type. For details, see 2.scripts/data_management/ign/bdalti_consolidate.py
****NOTE****

BDALTIV2_DALLES_25M_ASC_LAMB93_IGN69_FR
  Tile boundary polygons for each department

BDALTIV2_DST_25M_ASC_LAMB93_IGN69_FR
  Distance to the known points that were used to calculate the pixel's elevation
      0 = < 1 m
  1-249 = the distance in m
    250 = >= 250 m
    253 = not applicable
    254 = connecting pixel
    255 = no elevation

BDALTIV2_METADONNEES_25M_ASC_LAMB93_IGN69_FR
  Metadata for each department

BDALTIV2_MNT_25M_ASC_LAMB93_IGN69_FR
  Elevation (m) above mean sea level

BDALTIV2_SRC_25M_ASC_LAMB93_IGN69_FR
  Primary data source for the pixel's elevation (see DC_BDALTI_2-0.pdf)
```

#### BDTOPO_2-2_HYDROGRAPHIE_SHP_LAMB93_FXX_2017-10-02

Hydrographic data from BDTOPO version 2.2
Version 2.2
For mapping at 1:5000 to 1:50000 scale
EPSG:2154 (RGF93 / Lambert-93)
http://professionnels.ign.fr/bdtopo-hydrographie

#### BDTOPO_2-2_TOUSTHEMES_SQL_LAMB93_FRAN_2017-10-02

French buildings, roads, water bodies, etc.
Version 2.2
For mapping at 1:5000 to 1:50000 scale, positional error generally < 10 m
EPSG:2154 (RGF93 / Lambert-93)
http://professionnels.ign.fr/bdtopo

#### CONTOURS-IRIS_2-1__SHP__FRA_2017-06-30

INSEE IRIS census areas
2016 version 2.1
EPSG:2154 (RGF93 / Lambert-93)
http://professionnels.ign.fr/contoursiris

### insee

Data from the Institut national de la statistique et des études économiques (Insee)
https://www.insee.fr

```
documentation.md
  Description of the 1 km and 200 m gridded population datasets from the Insee website
  https://www.insee.fr/fr/statistiques/2520034#documentation

donnee-carroyees-documentation-generale.pdf
  Description of grid reference and spatial estimation methodology
  https://www.insee.fr/fr/statistiques/2520034#documentation
```

#### 1km-carreaux-metropole

Gridded estimate of 2010 population of France
1 km resolution
EPSG:3035 (ETRS89 / LAEA Europe)
https://www.insee.fr/fr/statistiques/1405815

```
Lisezmoi_donneescarroyees.txt
  README file

Liste variables métropole.pdf
  Description of the variables in R_rfl09_LAEA1000.*

R_rfl09_LAEA1000.dbf
  Population and southwest corner latitude / longitude of each grid cell

R_rfl09_LAEA1000.mi*
  Mapinfo files of grid cells and their population and latitude / longitude
  NOTE: these polygons do not align with the LAEA coordinates in R_rfl09_LAEA1000.dbf. They appear
  ~ 2 m west and ~ 1 to 4 m south. Maybe due to varying definition of "Lambert II étundu" CRS?
```

#### 200m-carreaux-metropole

Gridded estimate of 2010 population of France
200 x 200 m resolution
EPSG:3035 (ETRS89 / LAEA Europe)
https://www.insee.fr/fr/statistiques/2520034

```
car_m.dbf
  Population and southwest corner latitude / longitude of each grid cell

car_m.mi*
  Grid cells and their population in Mapinfo format
  NOTE: the polygons in these files do not align with the LAEA coordinates in car_m.dbf. They are
  ~ 2 m west and ~ 1 to 4 m south. Maybe due to varying definitions of "Lambert II étundu" CRS?

dictionnaire_des_variables.md
  Variable descriptions copied from https://www.insee.fr/fr/statistiques/2520034#dictionnaire

documentation-complete-donnees-a-200m.pdf
  Detailed description of estimate methodology, file contents, and instructions for use

documentation-synthetique-donnees-a-200m.pdf
  Summary of estimate methodology, file contents, and instructions for use
```

### joly_climats_france

Climatic types of France from Joly, D., Brossard, T., Cardot, H., Cavailhes, J., Hilal, M., & Wavresky, P. (2010). Les types de climats en France, une construction spatiale. Cybergeo. https://doi.org/10.4000/cybergeo.23155

Must credit in any derived works as follows:
  Source : Base de données climatiques communales 2009. THEMA Université de Franche-Comté, CNRS UMR6049 (F-25000 Besançon) /CESAER INRA UMR1041 (F-21000 Dijon) ; d’après Météo France 1971-2000.

```
Typologie des climats français.asc
  250 x 250 m resolution
  https://journals.openedition.org/cybergeo/23155

  Joly (2010) states that this data is in Lambert II étendu, but ASC files do not store projection information. If you map the data in Lambert II étendu, you will see that the pixels are far north of where they should be. I settled on a correction of 1000250 m to the south and 250 m to the east, which seemed to give a overall fit to the borders of France, and could be explained if somehow all the coordinates were offset by 10000000 m to the north and the coordinates of the bottom right corner of each pixel were saved as the coordinates of the top left corner.
```

### kloog_france_temperature

Output from the Kloog et al. (2016) temperature model for France
https://doi.org/10.1002/joc.4705

```
temp_kloog_1km_YYYY.nc
  Modeled daily mean air temperature for the year YYYY
  1 km resolution
  Lambert II étendu - see https://grasswiki.osgeo.org/wiki/IGNF_register_and_shift_grid_NTF-RGF93
  Daily 2001-01-01 - 2011-12-31
```

### landsat

Data from the USGS Landsat satellites
https://landsat.usgs.gov/

```
landsat-X
  Landsat 5 / 7 / 8 at-satellite brightness temperature and NDVI
  30 m resolution
  UTM (Zones 30-32 U-T)

wrs2
  france_for_landsat.shp
    A polygon with < 30 vertices that intersects all WRS2 tiles that intersect continental France
    Used to search for landsat scenes on https://earthexplorer.usgs.gov/
    Created in ArcGIS 10.5.1
    * Draw a polygon with < 30 vertices that intersects all features in france_wrs2_tiles.shp

  france_wrs2_tiles.shp
    All WRS2 tiles that intersect continental France
    Created in ArcGIS 10.5.1
    * Spatial intersect wrs2_asc_desc.shp with 1.work/spatial/borders/france_4326.shp

  wrs2_asc_desc.shp
    Worldwide Reference System (WRS) version 2 scene boundary shapefiles
    https://landsat.usgs.gov/pathrow-shapefiles

LANDSAT_X.xlsx
  Details of all Landsat X Collection 1 Level-1 Precision and Terrain (L1TP) corrected scenes that intersect France from January 1 2000 to December 31 2016
  Downloaded from https://earthexplorer.usgs.gov/ after searching with wrs2/france_for_landsat.shp
```

##### To order

See `2.scripts/data_management/landsat/` for order files and download tool.

1. Search for landsat data [EarthExplorer](https://earthexplorer.usgs.gov/)
   * Only search for Collection 1 L1TP scenes from 2000 to 2016
   * Use `ordering/wrs2/france_for_landsat.shp` to define the search area
2. Download the list of matching scenes (`2.scripts/data_management/landsat/LANDSAT_X.xlsx`)
3. Copy the product identifiers to a text file (`2.scripts/data_management/landsat/order_landsat_X.txt`)
4. Order the data from the [ESPA Bulk Order interface](https://espa.cr.usgs.gov/ordering/status/)
   * Upload the text file of product identifiers
   * Select the Level 2 products "Brightness Temperature" and "NDVI" (under "Spectral Indices")
   * Choose GeoTIFF output, do not reproject
   * Submit order
5. Downloaded completed order using [espa-bulk-downloader](https://github.com/USGS-EROS/espa-bulk-downloader)

##### File naming convention

```
  LX0S_L1TP_PPPRRR_YYYYMMDD_yyyymmdd_01_T1
    L = landsat
    X = sensor (C = OLI & TIRS, O = OLI, T = TIRS (Landsat 8) or TM (Landsat 5), E = ETM+)
    SS = satellite (5 / 6 / 8)
    L1TP = level-1 precision and terrain corrected
    PPP = WRS2 path
    RRR = WRS2 row
    YYYYMMDD = acquisition date
    yyyymmdd = processing date
    01 = collection 1
    T1 = tier 1
```


### meteo_france

Data from Meteo France, the French national meteorological service
http://www.meteofrance.com/

#### observations

Daily meteorological station observations
EPSG:4326 (WGS84)
Daily 2000-01-01 - 2016-12-31 (varies by station)
https://publitheque.meteo.fr/okapi/accueil/okapiWebPubli/index.jsp

Note that meteo france publitheque station details may conflict with the data:

* Station 38095001 = Chatte
  * Publitheque reports station moved and changed type from 3 to 2 on 2016-04-07
  * Data lat / lon corresponds to the "new" location starting 2015-01-01
  * Conclusion: hope the data is correct b/c there is no way to download station details from publitheque

```
regionX
  Daily observations from stations in region X (1 - 7)
  Provided by Meteo France

  donnees_regionX_YYYYMMDD_YYYYMMDD.txt
    Semicolon delimited text without header

station_types_by_year
  Annual lists of all stations with type 0-2 that were open for the entire year. Note that the
  station type listed in the file is *NOT* the type of the station during the year, but rather its
  current type (or last type if the station later closed). The Meteo France publitheque does not
  provide a way to export the type of a station for a particular year.

  stations_0-2_2000-2017.csv
    All stations that were of type 0-2 and open for an entire year at some point from 2000 to 2017.

  stations_0-2_YYYY.csv
    All stations that were of type 0-2 and open for the entire year.


meteo_obs_file_layout.csv
  Description and R data type of the fields in the data files
  Used to ensure proper typecasting when loading data in R

reseau_poste_codes.csv
  Station network (reseau poste) code descriptions from 'explications' sheet of type_stations.xls

station_metadata.csv
  Station metadata downloaded from Meteo France publitheque ('Tout sur les stations')
  More recent than the information in station_type_reseau.xls
  Lists 12296 stations, of which 3642 are also listed in station_type_reseau.csv (8654 are not)
  https://publitheque.meteo.fr/okapi/accueil/okapiWebPubli/index.jsp

station_metadata_file_layout.csv
  Description and R data type of the fields in station_metadata.csv

station_type_reseau.csv
  Station type and network compiled from type_stations.xls
  Older than the information in station_metadata.csv
  Lists 3664 stations, of which 3642 are also listed in station_metadata.csv (43 are not)

temperature_stations.csv
  List of all stations that measure temperature downloaded from Meteo France Publitheque
  Lists 5095 stations, which suggests that many of the stations in station_metadata.csv do not actually measure temperature. Of the 5095 stations, 2296 also appear in station_type_reseau.csv.

type_poste_codes.csv
  Station type (type poste) codes as listed on Meteo France publitheque ('Tout sur les stations')
  https://publitheque.meteo.fr/okapi/accueil/okapiWebPubli/index.jsp

type_reseau_notes_2016_11_15.docx
  Notes on station types and networks from Johanna's conversation with Annick Auffray

type_stations.xls
  Station type (type poste) and network (reseau poste) provided by Annick Auffray of Météo France
```

#### sim

Output of the Meteo France SAFRAN-ISBA-MODCOU (SIM) surface model.
8 km resolution
Lambert II étendu - see https://grasswiki.osgeo.org/wiki/IGNF_register_and_shift_grid_NTF-RGF93
Daily 2000-01-01 - 2016-12-31
https://donneespubliques.meteofrance.fr/?fond=produit&id_produit=230&id_rubrique=40

NOTE: If in ArcGIS Lambert II étendu data appears offset to the west, it may be because ArcGIS is
using a GCS with a prime meridian of 0 (Greenwich) rather than prime meridian of 2.337229166666667
(Paris). Try using "NTF (Paris) Lambert Zone II" rather than "NTF Lambert Zone II".

```
SIM2_YYYY_ZZZZ.csv
  Daily SIM model output for years YYYY to ZZZZ
  Semicolon delimited text with header
  Downloaded from https://publitheque.meteo.fr/okapi/accueil/okapiWebPubli/index.jsp

DD_SIM-catalogue.pdf
  Description of the SIM model and the output variables
  https://publitheque.meteo.fr/okapi/accueil/okapiWebPubli/index.jsp

sim2_file_format.csv
  Description and R data type of the fields in the data files
  Used to ensure proper typecasting when loading data in R

sim2_parametres.txt
  Description of the fields in the data files from Météo France publithèque
  https://publitheque.meteo.fr/okapi/accueil/okapiWebPubli/index.jsp
```

### modis

Data from the MODIS instrument on NASA's Aqua and Terra satellites
https://lpdaac.usgs.gov/dataset_discovery/modis

```
modis_lst-mod11_guide.pdf
  User guide for MODIS MXD11 LST data
  https://lpdaac.usgs.gov/sites/default/files/public/product_documentation/mod11_user_guide.pdf

modis_vegetation_guide.pdf
  User guide for MODIS vegetation indices (e.g. NDVI)
  https://lpdaac.usgs.gov/sites/default/files/public/product_documentation/mod13_user_guide.pdf
```

##### File naming convention

```
  MXDPPPP.AYYYYDDD.hHHvVV.006.yyyydddhhmmss.hdf
    MXD = satellite (MYD = aqua, MOD = terra)
    PPPP = product (11A1 = daily 1 km LST + emissivity, 13A3 = monthly 1 km NDVI)
    AYYYYDDD = acquisition date (year + day of year)
    HH = horizontal tile coordinate
    VV = vertical tile coordinate
    006 = version 6
    yyyydddhhmmss = production date
```

##### LST QC codes

As described in `modis-lst-mod11_guide.pdf`

```
xxxxxx00 =   0 = LST produced, good
xxxxxx01 =   1 = LST produced, not good
xxxxxx10 =   2 = no LST (cloud)
xxxxxx11 =   3 = no LST (other)

xxxx00xx =   0 + x = good quality
xxxx01xx =   4 + x = other quality
xxxx10xx =   8 + x = -
xxxx11xx =  12 + x = -

xx00xxxx =   0 + x = emiss err <= 0.01
xx01xxxx =  16 + x = emiss err <= 0.02
xx10xxxx =  32 + x = emiss err <= 0.04
xx11xxxx =  48 + x = emiss err >  0.04

00xxxxxx =   0 + x = LST err <= 1K
01xxxxxx =  64 + x = LST err <= 2K
10xxxxxx = 128 + x = LST err <= 3K
11xxxxxx = 192 + x = LST err >  3K

  0 = 00000000 =              = LST good, emiss err <= 0.01, LST err <= 1K
  2 = 00000010 =              = no LST (cloud)
  3 = 00000011 =              = no LST (other)

  5 = 00000101 =        4 + 1 = LST, but emiss err <= 0.01, LST err <= 1K
 17 = 00010001 =       16 + 1 = LST, but emiss err <= 0.02, LST err <= 1K
 33 = 00100001 =       32 + 1 = LST, but emiss err <= 0.04, LST err <= 1K
 49 = 00110001 =       48 + 1 = LST, but emiss err >  0.04, LST err <= 1K

 65 = 01000001 =  64 +      1 = LST, but emiss err <= 0.01, LST err <= 2K
 81 = 01010001 =  64 + 16 + 1 = LST, but emiss err <= 0.02, LST err <= 2K
 97 = 01100001 =  64 + 32 + 1 = LST, but emiss err <= 0.04, LST err <= 2K
113 = 01110001 =  64 + 48 + 1 = LST, but emiss err >  0.04, LST err <= 2K

129 = 10000001 = 128 +      1 = LST, but emiss err <= 0.01, LST err <= 3K
145 = 10010001 = 128 + 16 + 1 = LST, but emiss err <= 0.02, LST err <= 3K
161 = 10100001 = 128 + 32 + 1 = LST, but emiss err <= 0.04, LST err <= 3K
177 = 10110001 = 128 + 48 + 1 = LST, but emiss err >  0.04, LST err <= 3K

193 = 11000001 = 192 +      1 = LST, but emiss err <= 0.01, LST err >  4K
209 = 11010001 = 192 + 16 + 1 = LST, but emiss err <= 0.02, LST err >  4K
225 = 11100001 = 192 + 32 + 1 = LST, but emiss err <= 0.04, LST err >  4K
241 = 11110001 = 192 + 48 + 1 = LST, but emiss err >  0.04, LST err >  4K
```

#### Aqua

##### MYD11A1.006

Aqua daily 1 km land surface temperature and emissivity
Version 6
~ 1 km (926.6254 m)
SR-ORG:6842 (MODIS Sinusoidal)
Daily 2002-07-04 - 2016-12-31
https://lpdaac.usgs.gov/dataset_discovery/modis/modis_products_table/myd11a1_v006

##### MYD13A3.006

Aqua monthly 1 km vegetation indices
Version 6
~ 1 km (926.6254 m)
SR-ORG:6842 (MODIS Sinusoidal)
Monthly 2002-07 - 2016-12
https://lpdaac.usgs.gov/dataset_discovery/modis/modis_products_table/myd13a3_v006

#### Terra

##### MOD11A1.006

Terra daily 1 km land surface temperature and emissivity
Version 6
~ 1 km (926.6254 m)
SR-ORG:6842 (MODIS Sinusoidal)
Daily 2000-02-24 - 2016-12-31
https://lpdaac.usgs.gov/dataset_discovery/modis/modis_products_table/mod11a1_v006

##### MOD13A3.006

Terra monthly 1 km vegetation indices
Version 6
~ 1 km (926.6254 m)
SR-ORG:6842 (MODIS Sinusoidal)
Monthly 2000-02 - 2016-12
https://lpdaac.usgs.gov/dataset_discovery/modis/modis_products_table/mod13a3_v006

## Metadata for `1.work` (processed and derived data)

### borders

Borders of continental France and variations thereof.

```
france_buf1km_epsg-XXXX.shp
  Borders of France expanded by a 1 km buffer, in the EPSG:XXXX projection. Used for cropping
  datasets. The 1 km buffer ensures the expanded borders will entirely contain all MODIS 1 km LST
  grid cells. The borders are smoothed to reduce filesize by buffering by 25 km then -24 km.
  Created in ArcGIS 10.5.1
  * france_epsg-3035.shp
    - Buffer by 25 km
    - Buffer by -24 km
    - (Project to EPSG:2154 and EPSG:4326)

france_epsg-XXXX.shp
  Borders of France in EPSG:XXXX
  Created in ArcGIS 10.5.1
  * 0.raw/ign/ADMINEXPRESS_1-0__SHP_LAMB93_FXX_2017-04-18/ADMINEXPRESS/1_DONNEES_LIVRAISON_2017-04-18/ADE_1-1_SHP_LAMB93_FR/REGION.shp
    - Delete Corsica
    - Dissolve all regions
    - (Project to EPSG:3035 and EPSG:4326)

france_land_sea_epsg-XXXX.shp
  Expanded borders of France classified as land vs sea. Used to zero the elevation of ocean areas.
  Created in ArcGIS 10.5.1
  * france_buf1km_epsg-2154.shp
    - Union france_epsg-2154.shp
    - Digitize land / sea boundary at north, southwest, south, and southeast corners of France
    - Assign land attribute (land = 1, sea = 0)
    - Dissolve on land attribute
    - (Project to EPSG:3035 and EPSG:4326)
```

### climate_types

Climate types of France from Joly (2010) - see `2.scripts/preprocess/format_climate_types.R`

### elevation

Elevation datasets for continental France

##### astgtm2

ASTER global DEM v2 30 m elevation for continental France
Derived from `0.raw/aster/ASTGTM.002`

```
ASTGTM.002.gdb
  ArcGIS geodatabase containing raster mosaics linked to the tiles in 0.raw/aster/ASTGTM.002

astgtm2_france_dem.shp
  ASTGTM2 30 m elevation mosaiced and clipped to continental France (with a 1 km buffer)
  Sea elevations have been zeroed
  Created in ArcGIS 10.5.1
  * Raster Processing - Clip
    - Input raster = 1.work/spatial/elevation/astgtm2/ASTGTM.002.gdb/ASTGTM2_france_dem
    - Output extent = 1.work/spatial/borders/france_buf1km_epsg-4326.shp
    - Use Input Features for Clipping Geometry
  * Times
    - Input raster 1 = output of clip
    - Input raster 2 = 1.work/spatial/elevation/astgtm2/france_sea_mask_astgtm2.tif

astgtm2_france_num.shp
  ASTGTM2 QA data mosaiced and clipped to continental France (with a 1 km buffer)
    > 0 = number of ASTER stereo scene pairs used to determine pixel elevation
    < 0 = source of non-ASTER elevation fill data (see astgtm2_readme.pdf for details)
  Created in ArcGIS 10.5.1
  * Raster Processing - Clip
    - Input raster = 1.work/spatial/elevation/astgtm2/ASTGTM.002.gdb/ASTGTM2_france_num
    - Output extent = 1.work/spatial/borders/france_buf1km_epsg-4326.shp
    - Use Input Features for Clipping Geometry

france_sea_mask_astgtm2.tif
  Land / sea mask used to zero sea elevations
  Created in ArcGIS 10.5.1
  * Polygon to Raster
    - Input features = 1.work/spatial/borders/france_land_sea_epsg-4326.shp
    - Cellsize = 0.raw/aster/ASTGTM.002/ASTGTM2_N41E008_dem.tif
    - Environments - Processing Extent - Snap Raster = 0.raw/aster/ASTGTM.002/ASTGTM2_N41E008_dem.tif

astgtm2_readme.pdf
  NASA readme for ASTER GDEM 2 data
```

#### bdalti_v2-0

BDALTIV2 25 m elevation for continental France
Derived from `0.raw/ign/BDALTIV2_2-0_25M_ASC_LAMB93-IGN69`

```
BDALTIV2_2-0_25M.gdb
  ArcGIS geodatabase containing raster mosaics linked to the tiles in 0.raw/0.raw/ign/BDALTIV2_2-0_25M_ASC_LAMB93-IGN69

BDALTIV2_DST_25M_LAMB93_IGN69_france.tif
  Distance to the known points that were used to calculate the pixel's elevation
        0 = < 1 m
    1-249 = the distance in m
      250 = >= 250 m
      253 = not applicable
      254 = connecting pixel
      255 = no elevation
  Created in ArcGIS 10.5.1
  * Raster Processing - Clip
    - Input raster = 1.work/spatial/elevation/bdalti_v2-0/BDALTIV2_25M_FXX_DST_LAMB93_IGN69
    - Output extent = 1.work/spatial/borders/france_buf1km_epsg-2154.shp
    - Use Input Features for Clipping Geometry

BDALTIV2_MNT_25M_LAMB93_IGN69_france.tif
  ASTGTM2 25 m elevation mosaiced and clipped to continental France (with a 1 km buffer)
  Sea elevations have been zeroed
  Created in ArcGIS 10.5.1
  * Raster Processing - Clip
    - Input raster = 1.work/spatial/elevation/bdalti_v2-0/BDALTIV2_25M_FXX_MNT_LAMB93_IGN69
    - Output extent = 1.work/spatial/borders/france_buf1km_epsg-2154.shp
    - Use Input Features for Clipping Geometry
    * Raster Calculator
      - Con(clipped < 0, Int(clipped - 0.5), Int(clipped + 0.5)) * 1.work/spatial/elevation/bdalti_v2-0/france_sea_mask_bdaltiv2_25m.tif

BDALTIV2_SRC_25M_LAMB93_IGN69_france.tif
  Primary data source for the pixel's elevation (see 0.raw/ign/BDALTIV2_2-0_25M_ASC_LAMB93-IGN69_DXXX/DC_BDALTI_2-0.pdf)
  Created in ArcGIS 10.5.1
  * Raster Processing - Clip
    - Input raster = 1.work/spatial/elevation/bdalti_v2-0/BDALTIV2_25M_FXX_SRC_LAMB93_IGN69
    - Output extent = 1.work/spatial/borders/france_buf1km_epsg-2154.shp
    - Use Input Features for Clipping Geometry

france_sea_mask_bdaltiv2.tif
  Land / sea mask used to zero sea elevations
  Created in ArcGIS 10.5.1
  * Polygon to Raster
    - Input features = 1.work/spatial/borders/france_land_sea_epsg-4326.shp
    - Cellsize = 0.raw/BDALTIV2_2-0_25M_ASC_LAMB93-IGN69_DXXX/MNT/BDALTIV2_25M_FXX_0075_6850_MNT_LAMB93_IGN69.tif
    - Environments - Processing Extent - Snap Raster = 0.raw/BDALTIV2_2-0_25M_ASC_LAMB93-IGN69_DXXX/MNT/BDALTIV2_25M_FXX_0075_6850_MNT_LAMB93_IGN69.tif
```

#### eu-dem_v1-0

EU-DEM v1.0 25 m elevation for continental france
Derived from `0.raw/copernicus/eu-dem/eu-dem_v1-0`

```
eu-dem_v1-0.gdb
  ArcGIS geodatabase containing raster mosaics linked to the tiles in 0.raw/copernicus/eu-dem/eu-dem_v1-0

eu_dem_v10_france.tif
  EU-DEM v1.0 25 m elevation data mosaiced and clipped to continental France (with a 1 km buffer)
  Sea elevations have been zeroed
  Created in ArcGIS 10.5.1
  * Raster Processing - Clip
    - Input raster = 1.work/spatial/elevation/eu-dem_v1-0/eu-dem_v1-0.gdb/EUD_CP_DEMS
    - Output extent = 1.work/spatial/borders/france_buf1km_epsg-3035.shp
    - Use Input Features for Clipping Geometry
  * Raster Calculator
    - Con(clipped < 0, Int(clipped - 0.5), Int(clipped + 0.5)) * 1.work/spatial/elevation/eu-dem_v1-0/france_sea_mask_eu_dem_v10.tif

france_sea_mask_eu_dem_v10.tif
  Land / sea mask used to zero sea elevations
  Created in ArcGIS 10.5.1
  * Polygon to Raster
    - Input features = 1.work/spatial/borders/france_land_sea_epsg-3035.shp
    - Cellsize = 0.raw/copernicus/eu-dem/eu-dem_v1-0/EUD_CP-DEMS_3500025000-AA.tif
    - Environments - Processing Extent - Snap Raster = 0.raw/copernicus/eu-dem/eu-dem_v1-0/EUD_CP-DEMS_3500025000-AA.tif
```

#### eu-dem_v1-1

EU-DEM v1.1 25 m elevation for continental france
Derived from `0.raw/copernicus/eu-dem/eu-dem_v1-1`

```
eu-dem_v1-1.gdb
  ArcGIS geodatabase containing raster mosaics linked to the tiles in 0.raw/copernicus/eu-dem/eu-dem_v1-1

eu_dem_v11_france.tif
  EU-DEM v1.1 25 m elevation data mosaiced and clipped to continental France (with a 1 km buffer)
  Sea elevations have been zeroed
  Created in ArcGIS 10.5.1
  * Raster Processing - Clip
    - Input raster = 1.work/spatial/elevation/eu-dem_v1-0/eu-dem_v1-1.gdb/eu_dem_v11
    - Output extent = 1.work/spatial/borders/france_buf1km_epsg-3035.shp
    - Use Input Features for Clipping Geometry
  * Raster Calculator
    - Con(clipped < 0, Int(clipped - 0.5), Int(clipped + 0.5)) * 1.work/spatial/elevation/eu-dem_v1-1/france_sea_mask_eu_dem_v11.tif

france_sea_mask_eu_dem_v10.tif
  Land / sea mask used to zero sea elevations
  Created in ArcGIS 10.5.1
  * Polygon to Raster
    - Input features = 1.work/spatial/borders/france_land_sea_epsg-3035.shp
    - Cellsize = 0.raw/copernicus/eu-dem/eu-dem_v1-1/eu_dem_v11_E30N20.TIF
    - Environments - Processing Extent - Snap Raster = 0.raw/copernicus/eu-dem/eu-dem_v1-1/eu_dem_v11_E30N20.TIF
```

### extracts

Individual datasets extracted to one of the reference grids - see `2.scripts/extract/`
Landsat data is exported directly from Google Earth Engine: https://code.earthengine.google.com/?accept_repo=users/hough/P055_FR_Ta

### grids

The 1 km and 200 m reference grids for France

### joined

All datasets combined into a single table - see `2.scripts/extract/join_data.R`

### land_cover

Corine land cover data for continental France.

```
clc_20YY_france.tif
  Data for year 20YY
  Created in ArcGIS 10.5.1
  * 0.raw/copernicus/corine_land_cover/g100_clcYY_V18_5/g100_clcYY_V18_5.tif
  * Clip with 1.work/spatial/borders/france_buf1km_epsg-3035.shp

clc_legend.csv
  Table for converting from grid codes (pixel values) to CLC class codes
```

### landsat

Shapefiles to help find landsat data on https://earthexplorer.usgs.gov

### meteo_obs

#### observations

```
clean
  Meteo France station observations remaining after removing suspicious observations - see `2.scripts/preprocess/clean_meteo_data.R`

parsed
  Meteo France station observations parsed from the raw data files - see `2.scripts/preprocess/parse_meteo_data.R`

removed
  Meteo France station observations removed during the cleaning process - see `2.scripts/preprocess/clean_meteo_data.R`
```

### models

Fitted models - see `2.scripts/model/`

### population

Formatted INSEE population data - see `2.scripts/preprocess/format_insee_population.R`

### predictions

Model predictions - see `2.scripts/model/`

### sim

Formatted Meteo France SIM surface model data - see `2.scripts/preprocess/format_sim.R`

## Luke (CIMENT) setup

To submit a job:

```
oarsub -I --project epimed -p "network_address='luke41'" -l /core=1,walltime=1:00:00
```

### Installing R packages

#### Option 1: use fchuffar's conda packages on summer (do NOT mix with option 1)

In `~/.profile`, set $PATH and local R lib:

```
export PATH="/summer/epistorage/opt/bin:$PATH"
export PATH="/summer/epistorage/miniconda3/bin:$PATH"
export R_LIBS_USER="~/R/summer-library/%v"
```

Installing rgeos:

To install rgeos, you must specify the location of conda's lib dir so that
libgeos can be loaded. *But* the conda-intalled readline is improperly linked,
so you must use system libraries for everything except libgeos. The solution is
to specify `LD_LIBRARY_PATH=[sys_lib_dir]:[conda_lib_dir]` when installing:

```R
# Save the original LD_LIBRARY_PATH
original_ld_library_path <- Sys.getenv("LD_LIBRARY_PATH")

# Prefer system libraries, but use conda-installed libraries if necessary (i.e. for libgeos)
Sys.setenv(LD_LIBRARY_PATH = "/lib/x86_64-linux-gnu/:/summer/epistorage/miniconda3/lib")

# Install rgeos
install.packages("rgeos")

# Reset the original LD_LIBRARY_PATH
Sys.setenv(LD_LIBRARY_PATH = original_ld_library_path)
```

Installing rgdal (similar to rgeos):

```R
# Save the original LD_LIBRARY_PATH
original_ld_library_path <- Sys.getenv("LD_LIBRARY_PATH")

# Prefer system libraries, but use conda-installed libraries if necessary (i.e. for libgeos)
Sys.setenv(LD_LIBRARY_PATH = "/lib/x86_64-linux-gnu/:/summer/epistorage/miniconda3/lib")

# Install rgdal
install.packages("rgdal", configure.args = c(
  "--with-proj-share=/summer/epistorage/miniconda3/share/proj",
  "--with-data-copy=yes",
  "--with-proj-data=/summer/epistorage/miniconda3/share/proj"
))

# Install packages that depend on rgdal
install.packages("raster")
install.packages("gdalUtils")

# Reset the original LD_LIBRARY_PATH
Sys.setenv(LD_LIBRARY_PATH = original_ld_library_path)
```

If these don't work, try explicitly clearing $LD_LIBRARY_PATH:

```
export LD_LIBRARY_PATH=""
```

#### Option 2: use nix (do NOT mix with option 1)

In `~/.profile`, source nix and set local R lib:

```
source /applis/site/nix.sh
export R_LIBS_USER="~/R/nix-library/%v"
```

Install nix packages

```
nix-env -i libjpeg # for hdf4
nix-env -i szib    # for hdf4
nix-env -i zlib    # for gdal and hdf4
nix-env -i hdf
nix-env -i proj
nix-env -i geos
nix-env -i gdal
nix-env -i R-3.3.3
```

Install R packages

```R
install.packages("rgdal", configure.args = c(
  "--with-proj-include=/home/ihough/.nix-profile/include",
  "--with-proj-lib=/home/ihough/.nix-profile/lib"
))
```

## Mac setup

```
brew install hdf4
brew install hdf5
brew install netcdf
brew install proj

# Install GDAL from kyngchaos
brew cask install gdal-framework
# TODO investigate installing via homebrew from osgeo4mac tap - see https://github.com/OSGeo/homebrew-osgeo4mac
# note will need to modify to install hdf4 and netcdf
# brew install gdal --with-postgresql # --with-complete # but modify this to install only hdf4 and netcdf

# Install rgdal
install.packages("rgdal", configure.args = "--with-gdal-config=/Library/Frameworks/GDAL.framework/Programs/gdal-config")

# Install sf
install.packages("sf", configure.args = "--with-gdal-config=/Library/Frameworks/GDAL.framework/Programs/gdal-config")
```

## Potential improvements

m1: include distance to water / coast
m1 + m2: estimate values for grid cells missing climate_type or sim (and maybe NDVI?)
