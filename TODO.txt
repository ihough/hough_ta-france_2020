Calculate descriptive statistics (quantile(0, 0.1, 1, 5, 10, 25, 50, 75, 90, 95, 99, 99.9)) for all predictors and predictions

1. Get full station metadata for meteo france stations:
   * Type code for all stations
   * Date of type code change(s) for all stations
   * Precise (unrounded) location for all stations
   * Date of location change(s) for all stations
2. Incorporate improved metadata into observation cleaning (preprocess/clean_meteo_data.R)
