#!/usr/bin/env bash

# Sync some data files to CIMENT for Ron Sarafian's project

dest_dir="cargo:/bettik/PROJECTS/pr-edenpelagie/hough_ta_2020"
flags="-rt --delete-after --progress --chmod D0750 --chmod F0640"

# Run with -n or --dry-run to preview
while test $# -gt 0; do
  case "$1" in
    -n|--dry-run)
      flags="-n $flags"
      shift
      ;;
    *)
      echo "Unrecognized flag: $1"
      exit 1
      ;;
  esac
done

echo "Syncing to $dest_dir"
echo "Using rsync $flags"

# Echo commands before executing
set -v

# Sync dbs mod1 and mod3
rsync $flags ./1.work/joined/mod1_*_200[234]* $dest_dir
rsync $flags ./1.work/joined/mod3_200[234]* $dest_dir

# Sync models m1 and m2
rsync $flags ./1.work/models/m1_tmean_*_200[234]* $dest_dir
rsync $flags ./1.work/models/m2_tmean_200[234]* $dest_dir
